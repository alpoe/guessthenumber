package com.compassplus.guessthenumbergame;

import org.junit.Assert;
import org.junit.Test;

public class logicTest {

    @Test
    public void moverLimits_leftLimit(){
        logic lg = new logic(0,200);
        Assert.assertEquals(150, lg.moveLimit(true), 0);
    }
    @Test
    public void moverLimits_rightLimit(){
        logic lg = new logic(0,100);
        Assert.assertEquals(25, lg.moveLimit(false), 0);
    }

    @Test
    public void isNumberGuessedFalse(){
        logic lg = new logic(44,76);
        Assert.assertFalse(lg.isNumberGuessed());
    }
    @Test
    public void isNumberGuessedTrue(){
        logic lg = new logic(44,46);
        Assert.assertTrue(lg.isNumberGuessed());
    }
}
