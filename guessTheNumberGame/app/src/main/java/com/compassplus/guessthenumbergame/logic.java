package com.compassplus.guessthenumbergame;

public class logic {
    int right;
    int left;
    int mid;
    public logic(int left, int right){
        this.left = left;
        this.right = right;
        mid = Math.round((left + right)/2f);
    }

    public int moveLimit(boolean userAnswer){
        if (userAnswer == true){
            left = mid;
        } else{
            right = mid;
        }
        mid = Math.round((left + right)/2f);
        return mid;

    }

    public int getMid(){
        return mid;
    }
    public boolean isNumberGuessed(){
        if((right - left) <= 2){
            return true;
        }
        else{
            return false;
        }
    }
}
