package com.compassplus.guessthenumbergame;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends Activity {
    logic lg;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final TextView display = findViewById(R.id.text);
        final Button butYes = findViewById(R.id.but1);
        final Button butNo = findViewById(R.id.but3);
        final Button butOp = findViewById(R.id.but2);

        View.OnClickListener listenerYes = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!lg.isNumberGuessed()) {
                    display.setText(getString(R.string.question) + " " +lg.moveLimit(true) + "?");
                }
                else{
                    display.setText(getString(R.string.answer) + " " + lg.moveLimit(true));
                }
            }
        };
        View.OnClickListener listenerNo = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!lg.isNumberGuessed()) {
                    display.setText(getString(R.string.question) + " " +lg.moveLimit(false) + "?");
                } else{
                    display.setText(getString(R.string.answer) + " " + lg.moveLimit(false));
                }
            }
        };

        View.OnClickListener listenerOp = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(lg == null) {
                    lg = new logic(0,100);
                    display.setText(getString(R.string.question) + " " +lg.getMid() + "?");
                    butOp.setText(getString(R.string.finishStr));
                    butNo.setEnabled(true);
                    butYes.setEnabled(true);
                } else {
                    lg = null;
                    display.setText(getString(R.string.startTextView));
                    butNo.setEnabled(false);
                    butYes.setEnabled(false);
                    butOp.setText(getString(R.string.startStr));
                }
            }
        };

        butYes.setOnClickListener(listenerYes);
        butNo.setOnClickListener(listenerNo);
        butOp.setOnClickListener(listenerOp);
    }
}
